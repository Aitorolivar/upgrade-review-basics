// Crea una función llamada `findArrayIndex` que reciba como parametros un array de textos
// y un texto y devuelve la posición del array cuando el valor del array sea igual al 
// valor del texto que enviaste como parametro. Haz varios ejemplos y compruebalos.

// Sugerencia de función:

const creatures = ["Caracol", "Mosquito", "Salamandra", "Ajolote"];

function findArrayIndex(array, text) {
  for (let value of array) {
    if (value === text) {
      let index = array.indexOf(value);
      console.log(`Element: ${value} --> Position: ${index}`);
    } else {
      console.log('Error: not existing element');
    };
  };
};

findArrayIndex(creatures, 'Salamandra');
