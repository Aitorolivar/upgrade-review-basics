// **Iteración #6: Función swap**

// Crea una función llamada `swap()` que reciba un array y dos parametros que sean indices del array. 
// La función deberá intercambiar la posición de los valores de los indices que hayamos enviado como parametro. 
// Retorna el array resultante.

// Sugerencia de array:



let players = ["Mesirve", "Cristiano Romualdo", "Fernando Muralla", "Ronalguiño"];

function swap(arr, index1, index2) {
  let aux = "";
  aux = arr[index1];
  arr[index1] = arr[index2];
  arr[index2] = aux;
  return arr;
}
let result = swap(players, 1, 2);
console.log(result);

